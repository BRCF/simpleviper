// To parse the JSON, add this file to your project and do:
//
//   let coin = try? newJSONDecoder().decode(Coin.self, from: jsonData)

import Foundation

struct Coin: Codable {
    let status: Status?
    let data: [Datum]?
}

struct Datum: Codable {
    let id: Int?
    let name, symbol, slug: String?
    let circulatingSupply, totalSupply: Double?
    let maxSupply: Int?
    let dateAdded: String?
    let numMarketPairs: Int?
    let tags: [Tag]?
    let platform: Platform?
    let cmcRank: Int?
    let lastUpdated: String?
    let quote: Quote?
    
    enum CodingKeys: String, CodingKey {
        case id, name, symbol, slug
        case circulatingSupply = "circulating_supply"
        case totalSupply = "total_supply"
        case maxSupply = "max_supply"
        case dateAdded = "date_added"
        case numMarketPairs = "num_market_pairs"
        case tags, platform
        case cmcRank = "cmc_rank"
        case lastUpdated = "last_updated"
        case quote
    }
}

struct Platform: Codable {
    let id: Int?
    let name: Name?
    let symbol: Symbol?
    let slug: Slug?
    let tokenAddress: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name, symbol, slug
        case tokenAddress = "token_address"
    }
}

enum Name: String, Codable {
    case ethereum = "Ethereum"
    case omni = "Omni"
    case stellar = "Stellar"
}

enum Slug: String, Codable {
    case ethereum = "ethereum"
    case omni = "omni"
    case stellar = "stellar"
}

enum Symbol: String, Codable {
    case eth = "ETH"
    case omni = "OMNI"
    case xlm = "XLM"
}

struct Quote: Codable {
    let usd: Usd?
    let brl: Usd?
    enum CodingKeys: String, CodingKey {
        case usd = "USD"
        case brl = "BRL"
    }
}

struct Usd: Codable {
    let price, volume24H, percentChange1H, percentChange24H: Double?
    let percentChange7D, marketCap: Double?
    let lastUpdated: String?
    
    enum CodingKeys: String, CodingKey {
        case price
        case volume24H = "volume_24h"
        case percentChange1H = "percent_change_1h"
        case percentChange24H = "percent_change_24h"
        case percentChange7D = "percent_change_7d"
        case marketCap = "market_cap"
        case lastUpdated = "last_updated"
    }
}

enum Tag: String, Codable {
    case mineable = "mineable"
}

struct Status: Codable {
    let timestamp: String?
    let errorCode: Int?
    let errorMessage: JSONNull?
    let elapsed, creditCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case timestamp
        case errorCode = "error_code"
        case errorMessage = "error_message"
        case elapsed
        case creditCount = "credit_count"
    }
}

// MARK: Encode/decode helpers

class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
