//
//  Mappable.swift


import Foundation

protocol Mappable {
    var dateFormat: String { get }
    
    // Creates a new instance by decoding from the given data.
    init(from: Data) throws
    
    func getDate(withString dateString: String) -> Date?
}

extension Mappable where Self: Decodable {
    
    var dateFormat: String {
        return "yyyy-MM-dd'T'HH:mm:ssZ"
    }
    
    init(from data: Data) throws {
        self = try MappableHelper.decoder.decode(Self.self, from: data)
    }
    
    func getDate(withString dateString: String) -> Date? {
        return MappableHelper.getDateFormatter(withFormat: dateFormat).date(from: dateString)
    }
    
}
