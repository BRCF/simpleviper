//
//  MappableHelper.swift
import Foundation

class MappableHelper {
    
    // Just to make sure that a new dateFormatter is not being created every time
    // we decode a model.
    static var dateFormatter: DateFormatter = DateFormatter()
    static let decoder = JSONDecoder()
    
    static func getDateFormatter(withFormat format: String) -> DateFormatter {
        dateFormatter.dateFormat = format
        return dateFormatter
    }
    
}
