//
//  APIErrorHandle.swift
//  Pods
//
//  Created by Leandro Silva on 24/03/17.
//
//

//import UIKit
import Foundation

protocol APIErrorHandle {
    func handleHttpStatus(response: HTTPURLResponse?) -> Error?
}

extension APIErrorHandle {
    
    func handleHttpStatus(response: HTTPURLResponse?) -> Error? {
        
        guard let resp = response else { return nil }
        
        if resp.statusCode >= 200 && resp.statusCode <= 299 {
            return nil
        }
        
        return NSError(domain: "timDigital", code: resp.statusCode, userInfo: nil)
        
    }
    
}
