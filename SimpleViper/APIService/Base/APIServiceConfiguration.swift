//
//  APIServiceConfiguration.swift
//  Pods
//
//  Created by Leandro Silva on 24/03/17.
//
//

import Foundation
import Alamofire

protocol APIServiceConfiguration {
    var manager: Alamofire.SessionManager { get }
}

extension APIServiceConfiguration {
    
    var manager: Alamofire.SessionManager {
        let man = Alamofire.SessionManager.default
        man.session.configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        man.session.configuration.timeoutIntervalForRequest = 15.0
        man.session.configuration.httpAdditionalHeaders = [ "Accept-Encoding": "gzip;q=1.0,compress;q=0.5" ]
        return man
    }
    
}
