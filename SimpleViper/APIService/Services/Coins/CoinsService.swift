//
//  CoinsService.swift
//  SimpleViper
//
//  Created by Bruno Castilho on 08/01/19.
//  Copyright © 2019 Bruno Castilho. All rights reserved.
//

import Foundation

private protocol CoinsServiceProtocol {
    func fetchCoins(router:CoinsRouter, completion: @escaping(Result<Coin>)-> Void)
}

public struct CoinsService: CoinsServiceProtocol, APIServiceConfiguration,APIErrorHandle {
    func fetchCoins(router: CoinsRouter, completion: @escaping (Result<Coin>) -> Void) {
        self.manager.request(router).responseJSON { response -> Void in
            switch response.result {
            case .success(let value):
                do {
                    let coinsResult = try JSONDecoder().decode(Coin.self,withJSONObject: value, options: [])
                    completion(Result.success(coinsResult))
                } catch {
                    completion(Result.failure(error))
                }
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}

