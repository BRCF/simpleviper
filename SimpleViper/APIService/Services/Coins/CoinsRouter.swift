//
//  CoinsRouter.swift
//  SimpleViper
//
//  Created by Bruno Castilho on 08/01/19.
//  Copyright © 2019 Bruno Castilho. All rights reserved.
//

import Foundation
import Alamofire

public enum CoinsRouter:APIRouter {
    
    case getCoin()
    
    var token: String { return  "986344b9-ad19-41eb-9bd9-fdf7f1b05e86" }
    
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .getCoin():
            return APIConfig.Coin.coins
        }
    }
}
