//
//  ApiConfig.swift


import Foundation

enum APIConfig {
    static let baseApiUrl: String = "https://pro-api.coinmarketcap.com/"
    
    enum Coin {
        static let coins = "v1/cryptocurrency/listings/latest"
    }
}
