//
//  AppDelegate.swift
//  SimpleViper
//
//  Created by Bruno Castilho on 08/01/19.
//  Copyright © 2019 Bruno Castilho. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if let myWindow = self.window {
            CoinWireframe().presentIn(window:myWindow)
        }
        
        return true
    }
}

