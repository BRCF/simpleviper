//
//  RootWireframe.swift
//  ViperProject
//
//  Created by Bárbara Silveira on 23/03/17.
//  Copyright © 2017 Globosat Programadora. All rights reserved.
//

import Foundation
import UIKit

class RootWireframe : NSObject {
    func showRootViewController(viewController: UIViewController, inWindow: UIWindow) {
        let navigationController = navigationControllerFromWindow(window: inWindow)
        navigationController.viewControllers = [viewController]
    }
    
    func navigationControllerFromWindow(window: UIWindow) -> UINavigationController {
        let navigationController = window.rootViewController?.navigationController
        return navigationController!
    }
}
