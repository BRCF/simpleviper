//
//  Utilities.swift
//  ViperProject
//
//  Created by Bárbara Silveira on 30/03/17.
//  Copyright © 2017 Globosat Programadora. All rights reserved.
//

import UIKit
import Alamofire

func storyboardWith(name:String) -> UIStoryboard {
    let storyboard = UIStoryboard(name:name, bundle: Bundle.main)
    return storyboard
}
