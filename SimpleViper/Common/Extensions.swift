//
//  Extensions.swift
//  ViperProject
//
//  Created by Bárbara Silveira on 22/02/17.
//  Copyright © 2017 Globosat Programadora. All rights reserved.
//

import UIKit


protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return NSStringFromClass(self).components(separatedBy: ".").last ?? String(describing: Mirror(reflecting: self).subjectType)
    }
}

extension UITableViewCell: ReusableView {}
