//
//  CoinViewData.swift
//  SimpleViper
//
//  Created by Bruno Castilho on 09/01/19.
//  Copyright © 2019 Bruno Castilho. All rights reserved.
//

import Foundation
struct CoinViewData {
    var name:String
    var symbol:String
    var price:Double
    var priceText = ""
    
    init(name:String, symbol:String, price:Double) {
        self.name = name
        self.symbol = symbol
        self.price = price
        
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: self.price as NSNumber) {
            priceText = "\(formattedTipAmount)"
        }
    }
}

