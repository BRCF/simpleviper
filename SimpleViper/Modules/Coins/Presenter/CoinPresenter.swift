//
//  CoinPresenter.swift
//  SimpleViper
//
//  Created by Bruno Castilho on 09/01/19.
//  Copyright © 2019 Bruno Castilho. All rights reserved.
//

import Foundation

//MARK: - CoinPresenter
class CoinPresenter {
   
    var interface:CoinViewControllerInterface?
    let interactor:CoinInteractorProtocolInput
    let wireframe:CoinWireframe
    
    
     init(interactor:CoinInteractorProtocolInput, wireframe:CoinWireframe) {
        self.interactor = interactor
        self.wireframe = wireframe
    }
    
    func resolveViewDataWith(coin:Coin)->[CoinViewData]{
        var viewData = [CoinViewData]()
        guard let coinData = coin.data else {
            return viewData
        }
        for T in coinData {
            let coin = CoinViewData.init(name:T.name ?? "", symbol: T.symbol ?? "", price: T.quote?.usd?.price ?? 0.0)
            viewData.append(coin)
        }
        return viewData
    }
}


//MARK: - CoinPresenter/CoinInteractorOutput
extension CoinPresenter:CoinInteractorOutput {
    func fetchData(_ obj: Coin?) {
        if let objCoin = obj {
            self.interface?.getCoins(resolveViewDataWith(coin:objCoin))
        }else {
            self.interface?.errorScene()
        }
    }
}


//MARk: - Coinpresenter/ModuleCoinProtocol
extension CoinPresenter:ModuleCoinProtocol {
    func getData() {
       self.interactor.fetchData()
    }
}
