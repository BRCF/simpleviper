//
//  ModuleProtocol.swift
//  SimpleViper
//
//  Created by Bruno Castilho on 09/01/19.
//  Copyright © 2019 Bruno Castilho. All rights reserved.
//

import Foundation

protocol ModuleCoinProtocol {
    func getData()
}
