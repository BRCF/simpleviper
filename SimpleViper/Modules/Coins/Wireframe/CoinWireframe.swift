//
//  CoinWireframe.swift
//  SimpleViper
//
//  Created by Bruno Castilho on 09/01/19.
//  Copyright © 2019 Bruno Castilho. All rights reserved.
//

import Foundation
import UIKit

let identifier = "coinViewController"

class CoinWireframe {
    //lazy var
    weak var coinViewController:CoinViewController?
    
    func presentIn(window:UIWindow){
    
        let interactor = CoinInteractor.init(service:SLCoin())
        let presenter = CoinPresenter.init(interactor:interactor, wireframe:self)
        interactor.delegate = presenter
        
        coinViewController = storyboardWith(name:"CoinList").instantiateViewController(withIdentifier: identifier) as? CoinViewController
        coinViewController?.delegate = presenter
        presenter.interface = coinViewController
        window.rootViewController = UINavigationController(rootViewController: coinViewController!)
        window.makeKeyAndVisible()
    }
}
