//
//  CoinInteractor.swift
//  SimpleViper
//
//  Created by Bruno Castilho on 09/01/19.
//  Copyright © 2019 Bruno Castilho. All rights reserved.
//

import Foundation


//MARK: - CoinInteractorProtocol
protocol CoinInteractorOutput {
    func fetchData(_ obj:Coin?)
}

//MARK: CoinInteractorInterface
protocol CoinInteractorProtocolInput{
    //func convertTo(_ coin:String = "BRL")
    func fetchData()
}

//MARK: - Class CoinInteractor
class CoinInteractor:CoinInteractorProtocolInput {
    
    //MARK: PROPERTIES
    var delegate:CoinInteractorOutput?
    let service:SLCoin
    var coins:Coin?
    
    init(service:SLCoin) {
        self.service = service
    }
    
    //MARK: PUBLIC FUNCS
    func fetchData(){
        self.service.getCoins(completion: {
            self.delegate?.fetchData($0)
        }) {
            print("Error ")
        }
    }
}

