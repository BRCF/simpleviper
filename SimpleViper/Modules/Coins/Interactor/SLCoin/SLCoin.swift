//
//  SLCoin.swift
//  SimpleViper
//
//  Created by Bruno Castilho on 09/01/19.
//  Copyright © 2019 Bruno Castilho. All rights reserved.
//

import Foundation


//Enum errors etc

class SLCoin:NSObject {
    
    func getCoins(completion:@escaping (Coin?)->Void, errorCompletion:@escaping()->()){
        let router = CoinsRouter.getCoin()
        let service = CoinsService()
        
        service.fetchCoins(router:router) { (result) in
            if let coin = result.value {
               completion(coin)
            }else {
               errorCompletion()
            }
        }
    }
    
}
