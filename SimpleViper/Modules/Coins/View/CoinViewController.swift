    //
    //  CoinViewController.swift
    //  SimpleViper
    //
    //  Created by Bruno Castilho on 09/01/19.
    //  Copyright © 2019 Bruno Castilho. All rights reserved.
    //

    import UIKit

    class CoinViewController: UIViewController {
        
        //MARK: - PROPERTIES
        @IBOutlet weak var tableView: UITableView!
        var delegate:ModuleCoinProtocol?
        var viewData = [CoinViewData]()
        
        //MARK:- LIFE CYCLE
        override func viewDidLoad() {
            super.viewDidLoad()
            self.delegate?.getData()
        }
    }

    //MARK: - UITableViewDelegate
    extension CoinViewController:UITableViewDelegate {}
    
    //MARK: - UITableViewDataSource
    extension CoinViewController:UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return viewData.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier:"Cell")
            let objViewData = self.viewData[indexPath.row]
            cell?.textLabel?.text = "\(objViewData.name) / \(objViewData.symbol)"
            cell?.detailTextLabel?.text = objViewData.priceText
            return cell ?? UITableViewCell()
        }
    }
    
    //MARK: - CoinViewControllerProtocol
    extension CoinViewController:CoinViewControllerInterface {
        func errorScene() {
            print("Tela de erro")
        }
        
       func getCoins(_ coins:[CoinViewData]){
            self.viewData = coins
            self.tableView.reloadData()
        }
    }
    
   
    
    
    

